var mssql = require('mssql');

var db = {
    connection: null,
    setDbConnection: function(){
        connection = new mssql.Connection({
            user: 'ezsave',
            password: '!@34QWer',
            server: 'ezsave.database.windows.net',
            database: 'ezsave',
            options: {
                encrypt: true
            },
            pool: {
                max: 10,
                min: 0,
                idleTimeoutMillis: 30000
            }
        });
    },
    query: function(query, res){
        connection.connect().then(function(){
            new mssql.Request(connection).query(query).then(function(recordser){
                res.send(recordser);
            }).catch(function(err){
                res.status(404).send('query not found');
            });
        });
    }
};

module.exports = db;