var app = require('./server');
var queries = require('./queries');
var PORT = normalizePort(process.env.PORT || '8080');
var db = require('./db');

// const PORT=8888;

app.listen(PORT, function(){
    console.log('server up, listen on port ' + PORT);
});

app.use(function(req, res){
    console.log(req.url);

    var urlParts = req.url.slice(1).split('/');
    
    var query = queries;

    for(i in urlParts){
        query = query[urlParts[i]];
    }
    
    if(query == undefined || query == null || query == ''){
        res.status(404).send('query not found');
        return;
    }

    db.setDbConnection();

    db.query(query, res);
});

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

function onListening(){
    var addr = server.address();
    var bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    console.log('Listening on ' + bind);
}

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}